"""
========================================================
Ricardo
========================================================
"""
numeros: list = []
listaa = []
listab=[]
for n in range(1,101):
    numeros.append(n)
    if n<=50:
        listaa.append(n**2)
    else:
        listab.append(n**3)


print (numeros)
numerosa= numeros [:50]
print (numerosa)
numerosb= numeros [50:]
print (numerosb)
print ("______________________________________________")
print ("CUADRADO")
print (listaa)
print ("______________________________________________")
print ("CUBO")
print (listab)



"""
========================================================
-------------------Nelson-----------------
========================================================
"""
def createList(nMin: int, nMax: int)->list:
    numeros: list = []
    for n in range(nMin,nMax+1):
        numeros.append(n)
    return numeros


def sliceList(numeros: list, split: int)->list:
    lists: list = []
    list1: list = []
    list2: list = []
    index1: int = 0
    index2: int = 0
    for n in numeros:
        if n <= split:
            list1.append(n)
            index1 += 1
        else:
            list2.append(n)
            index2 += 1
    lists = [list1,list2]
    return lists


def expList(numeros: list, exp: int)->list:
    expList: list = []
    for n in numeros:
        expList.append(n**exp)
    return expList

#PROGRAMA============================================
fullList: list = createList(1,100)
semiList: list = sliceList(fullList, 50)

print(semiList[0])
print(".....................................")
print(expList(semiList[0], 2))
print("=====================================")
print(semiList[1])
print(".....................................")
print(expList(semiList[1], 3))



"""
========================================================
-------------------Ovidio-----------------
========================================================
"""

numeros: list = []
for n in range(1, 101):
    numeros.append(n)

lista1: list = numeros[0: 50]
lista2: list = numeros[50 :100]

print(lista1)
print(lista2)
c1 = 0
for n in lista1:
    lista1[c1] = n * n
    c1 += 1

print(lista1)
c2 = 0
for y in lista2:
    lista2[c2] = y ** 3
    c2 += 1



"""
========================================================
-------------------Marcela-----------------
========================================================
"""
numeros:list=[]
for n in range(1,101):
    numeros.append(n)
print("Lista total: ", numeros)

lista1:list=numeros[:50]
print("Primera lista: ", lista1)

lista2:list=numeros[50:]
print("Segunda lista: ", lista2)

cuadrado:list=[]
for c in lista1:
    cuadrado.append(c*c)
print("Lista de cuadrados: ", cuadrado)

cubo:list=[]
for c in lista2:
    cubo.append(c*c*c)
print("Lista de cubos: ", cubo)





"""
========================================================
----------Juan Manuel Vargas----------
========================================================
"""
lista = []
sub_lista_1 = []
sub_lista_2 = []
cuadrado = []
cubo = []
for x in range(1,101):
    lista.append(x)
print("Lista",lista)

lista_1 = lista[:50]
print("Lista 1 de los numeros del 1 hasta el 50: ",lista_1 )
lista_2 = lista[50:]
print("Lista 2 de lso numeros del 50 hasta el 100: ",lista_1 )


for x in lista_1:
    cuadrado.append(x ** 2)
print("Lista 1 al cuadrado: ", cuadrado)

for y in lista_1:
    cubo.append(y ** 3)
print("Lista 2 al cubo: ", cubo)


"""
========================================================
Paulo
========================================================
"""
#Lista de números
numeros: list = []
#Itera en un rango de 1 hasta 100 (101 lo excluye)
for n in range(1,101):
    #Añade cada valor del rango a la lista
    numeros.append(n)

#Imprime la lista de números
print(numeros)

#Crear una copia de mi lista
copia1_numeros = numeros[0:50].copy()
copia2_numeros = numeros[50:].copy()

index=0
for valor in copia1_numeros:
    copia1_numeros[index] = pow(valor,2)
    index += 1
print("\n------ Primera lista elevado al cuadrado ----------------\n")    
print(copia1_numeros)

index=0
for valor in copia2_numeros:
    copia2_numeros[index] = pow(valor,3)
    index += 1
print("\n------- Segunda lista elevado al cubo ----------------\n")        
print(copia2_numeros)