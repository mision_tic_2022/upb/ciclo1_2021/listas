
"""
========================================================
Ricardo
========================================================
"""
def mifunc()->list:
    estudiantes = []
    opc='S'
    while opc.upper() == 'S':
        estudiante = input("Por favor ingrese el nombre del estudiante: ")
        estudiantes.append(estudiante)
        opc = input("¿Desea continuar ingresando nombres de estudiantes? S (si) - N(no): ")
    return estudiantes 

lista= mifunc()
lista.sort()
print (lista)

"""
========================================================
Paulo
========================================================
"""
#Función para insertar un estudiante en una lista
def insertar_estudiante(lista: list, nombre_estudiante: str)->list:
    #inserta al final de la lista un nuevo elemento
    lista.append(nombre_estudiante)
    #Retorna la lista con el nuevo elemento insertado
    return lista

def buscar_estudiante(lista: list, nombre_estudiante: str)->bool:
    resp: bool = False
    #Iteramos la lista de estudiantes
    for n in lista:
        #Compara el estudiante a buscar con cada elemento de la lista
        if n.lower() == nombre_estudiante.lower():
            resp = True
    #Retornamos un booleano (True si encuentra al estudiante - False en caso contrario)
    return resp

def mostrar_estudiantes(lista: list)->list:
    i: int = 0
    for x in lista:
        print("Estudiante ", i+1, ": ", x)
        i += 1
    #return lista

def ordenar_estudiantes(lista: list)->list:
    #Ordenar lista ascendentemente
    lista.sort()
    print("Lista ordenada ascendente")
    print(lista)

def menu():
    #Creamos una lista vacia de estudiantes
    estudiantes: list = []
    opc_menu: int = -1
    while opc_menu != 0:
        opc = 'S'
        print("---------------------------------------------")
        print("1 --> Añadir nuevos estudiantes")
        print("2 --> Buscar un estudiante")
        print("3 --> Mostrar todos los estudiantes")
        print("4 --> Ordenar lista de estudiantes")
        print("0 --> Salir")
        print("---------------------------------------------")
        opc_menu = int( input("Por favor ingrese una opción: ") )

        if opc_menu == 1:
            """
            con .upper() convertimos una cadena a mayúscula
            Ejemplo: s.upper() -> S - hola.upper() -> HOLA
            """
            while opc.upper() == 'S':
                #Solicitamos el nombre del estudiante
                nombre_estudiante = input("Por favor ingrese el nombre del estudiante: ")
                #Inserto el nuevo elemento en la lista y sobreescribo la lista original
                estudiantes = insertar_estudiante(estudiantes, nombre_estudiante)
                #Preguntamos al usuario si quiere continuar ingresando nombres
                opc = input("¿Desea continuar ingresando nombres de estudiantes? S (si) - N(no): ")
        elif opc_menu == 2:
            #Solicitamos el nombre del estudiante a buscar
            nombre_estudiante = input("Nombre del estudiante que desea buscar: ")
            #Obtenemos un valor booleano de la búsqueda del estudiante
            resp = buscar_estudiante(estudiantes, nombre_estudiante)
            #Validamos si la búsqueda fue satisfactoria
            if resp:
                print("El estudiante existe en la lista")
            else:
                print("El estudiante no existe en la lista")
        elif opc_menu == 3:
            mostrar_estudiantes(estudiantes)
        elif opc_menu == 4:
            ordenar_estudiantes(estudiantes)

#Llamamos la función principal (menu())
menu()

"""
========================================================
Miguel
========================================================
"""
#Función para insertar un estudiante en una lista
def insertar_estudiante(lista: list, nombre_estudiante: str)->list:
    #inserta al final de la lista un nuevo elemento
    lista.append(nombre_estudiante)
    #Retorna la lista con el nuevo elemento insertado
    return lista
estudiantes: list = []
opc="S"
while opc.upper() == "S":
    #Solicitamos el nombre del estudiante
    nombre_estudiante = input("Por favor ingrese el nombre del estudiante: ")
    #Inserto el nuevo elemento en la lista y sobreescribo la lista original
    estudiantes = insertar_estudiante(estudiantes, nombre_estudiante)
    #Preguntamos al usuario si quiere continuar ingresando nombres
    opc = input("¿Desea continuar ingresando nombres de estudiantes? S (si) - N(no): ")
estudiantes.sort()
print(estudiantes)


"""
========================================================
Renata
========================================================
"""
lista: list = []

#Función para insertar un estudiante en una lista
def insertar_estudiante(lista: list, nombre_estudiante: str)->list:
    #inserta al final de la lista un nuevo elemento
    lista.append(nombre_estudiante)
    #Retorna la lista con el nuevo elemento insertado
    return lista

def mostrar_estudiantes(lista: list)->list:
    for n in lista:
        lista.sort()
        print (lista)
    
def menu():
    #Creamos una lista vacia de estudiantes
    estudiantes: list = []
    opc_menu: int = -1
    while opc_menu != 0:
        opc = 'S'
        print("---------------------------------------------")
        print("1 --> Añadir nuevos estudiantes")
        print("2 --> Mostrar la lista ordenada")
        print("0 --> Salir")
        print("---------------------------------------------")
        opc_menu = int( input("Por favor ingrese una opción: ") )

        if opc_menu == 1:
            """
            con .upper() convertimos una cadena a mayúscula
            Ejemplo: s.upper() -> S - hola.upper() -> HOLA
            """
            while opc.upper() == 'S':
                #Solicitamos el nombre del estudiante
                nombre_estudiante = input("Por favor ingrese el nombre del estudiante: ")
                #Inserto el nuevo elemento en la lista y sobreescribo la lista original
                estudiantes = insertar_estudiante(estudiantes, nombre_estudiante)
                #Preguntamos al usuario si quiere continuar ingresando nombres
                opc = input("¿Desea continuar ingresando nombres de estudiantes? S (si) - N(no): ")
        elif opc_menu == 2:
                    mostrar_estudiantes(estudiantes)

#Llamamos la función principal (menu())
menu()

"""
========================================================
Nelson
========================================================
"""
def getUser(users: list)->list:
    users.append(input("Por favor escriba el nombre del estudiante: "))
    return users

def sortListAZ(users: list)->list:
    users.sort()
    return users

def confirmation()->bool:
    option: bool = False
    try:
        option = bool(int(input("Desea continuar? SI(1) / NO(0): ")))    
    except:
        option = bool(int(input("Desea continuar? SI(1) / NO(0): "))) 
    return option

option: bool = True
userList: list = []

while option:
    userList = getUser(userList)
    option = confirmation()

userList = sortListAZ(userList)
print(userList)

"""
========================================================
Juan Manuel
========================================================
"""
def ordenador(n_estudiantes):
    a = 0
    n = 0
    estudiantes = []
    while a < n_estudiantes:
        a+= 1
        n += 1
        estudiantes.append(input(f"Estudiante{n}: ") )
        if a == n_estudiantes:
            continuar = input("¡Desea ingresar mas estudiantes?, S(si) N(no): ")
            if continuar == "S":
                a = a -1
            else:
                estudiantes.sort()
                print("Estudiantes ingresado y ordenados alfabeticamente:",estudiantes)
                exit()  

ordenador(n_estudiantes = int(input("Cuantos estudiantes vas a ingresar: ")))