"""
Ejercicio:
Crear una lista de números del 1 al 100,
A partir de la lista generada, crear dos sublistas
en la cual la primera contenga los números del 1 al 50.
la segunda lista contenga los elementos restantes.
Los elementos de la primera lista elevarlos al cuadrado y
los de la segunda lista elevarlos al cubo.
RECUERDEN:
Rebanar una lista: [inicio : fin] donde irá hasta: fin - 1
"""
#Lista de números
numeros: list = []
#Itera en un rango de 1 hasta 100 (101 lo excluye)
for n in range(1, 101):
    #Añade cada valor del rango a la lista
    numeros.append(n)

#Obtiene los elementos en las posiciones 0 hasta 50-1
lista_1: list = numeros[:50]
#Obtiene los elementos en las posiciones 50 hasta la última posicion
lista_2: list = numeros[50:]

#Elevar los elementos de la lista_1 al cuadrado
index = 0
for n in lista_1:
    #Actualizamos el elemento en la posicion index por elevado al cuadrado
    lista_1[index] = n**2
    #Actualizamos la posicion
    index += 1

#Elevar los elementos de la lista_2 al cubo
index = 0
for n in lista_2:
    lista_2[index] = n**3
    #index = index + 1
    index += 1

#Imprimimos la lista 2
print("Lista 1: ")
print(lista_1)
print("Lista 2:")
print(lista_2)

