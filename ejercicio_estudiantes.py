"""
Desarrolle por medio de funciones un programa 
que almacene los nombres de N estudiantes, esta información 
la debe de pedir por consola.
Al final del ejercicio imprima en orden alfabetico los nombres
de los estudiantes.
RECUERDEN:
Funcion: def nombre_funcion(parametros): return x
Ordenar una lista: mi_lista.sort()
mi_lista.sort(reverse=True)
"""

def agregar_estudiante(lista: list, nombre: str)->list:
    #Añade al final de la lista el nombre del estudiante
    lista.append(nombre)
    return lista

def solicitar_datos():
    estudiantes: list = []
    opc: str = 's'
    while opc == 's':
        nombre = input("Por favor ingrese el nombre del estudiante: ")
        estudiantes = agregar_estudiante(estudiantes, nombre)
        opc = input("¿Desea continuar? s(si) - n(no): ")
    estudiantes.sort()
    print(estudiantes)

solicitar_datos()
