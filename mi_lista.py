
#Lista con diferentes tipos de datos
lista: list = [5,2,"hola", True, 4.5]
#Añadimos un elemento a la lista
lista.append(10)
#Insertamos un elemento en la posición 0
#lista.insert(posición, valor_a_añadir)
lista.insert(0,8)
#Mostrar toda la lista
print(lista)

#Eliminar el último elemento
elemento_borrado = lista.pop()
print(lista)
print("El elemento eliminado es: ", elemento_borrado)
#Eliminar un elemento en un posición en específico
elemento_borrado = lista.pop(1)
print(lista)
print("El elemento eliminado es: ", elemento_borrado)
#reemplazar un elemento de la lista
lista[2] = "Fresa"
print(lista)
print("Cambiar de posición \"fresa\" ")
#Sacamos a 'fresa' de la lista
fruta = lista.pop(2)
#Insertamos el elemento eliminado en la posición cero (0)
lista.insert(0, fruta)
#Imprimimos la lista
print(lista)


"""
https://gitlab.com/mision_tic_2022/upb/ciclo1_2021
"""






#Mostrar un elemento en una posición
#print( lista[3] )
"""
#Iterar la lista
for x in lista:
    print(x)
"""
#Lista vacia
#lista: list = []